package com.vitu.riot.champion.shared.util;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class MetricsUtil {

    private static final String CREATE_CHAMPION_METRIC_NAME = "create_champion";
    private final MeterRegistry meterRegistry;

    public void incrementCreateChampionMetric() {
        log.info("Incrementing metric: {}", CREATE_CHAMPION_METRIC_NAME);
        meterRegistry.counter(CREATE_CHAMPION_METRIC_NAME).increment();
    }

}
