package com.vitu.riot.champion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RiotChampionApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(RiotChampionApiApplication.class, args);
    }

}
