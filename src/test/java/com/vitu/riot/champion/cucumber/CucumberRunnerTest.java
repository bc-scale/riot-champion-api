package com.vitu.riot.champion.cucumber;

import com.vitu.riot.champion.AbstractDatabaseTest;
import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;


@Suite
@SpringBootTest
@AutoConfigureMockMvc
@IncludeEngines("cucumber")
@CucumberContextConfiguration
@ActiveProfiles(profiles = "test")
@SelectClasspathResource("features")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "com.vitu.riot.champion.cucumber")
public class CucumberRunnerTest extends AbstractDatabaseTest {
}
