package com.vitu.riot.champion.cucumber.steps;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vitu.riot.champion.domain.Champion;
import com.vitu.riot.champion.repository.ChampionRepository;
import com.vitu.riot.champion.web.dto.ChampionDto;
import com.vitu.riot.champion.web.dto.SkillDto;
import com.vitu.riot.champion.web.dto.SkinDto;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Slf4j
public class CriarCampeaoStepdefs extends BasicStepDefs {

    @Autowired
    ChampionRepository championRepository;

    @Dado("que não exista o campeão de nome {string}")
    public void queNaoExistaOhCampeaoDeNome(String name) {
        log.info(name);
        championRepository.findByNameIgnoreCase(name).ifPresent(champion -> championRepository.delete(champion));
    }

    @Quando("for feita a requisição do tipo {string} para criar um novo campeão com o seguinte corpo:")
    public void forFeitaARequisicaoDoTipoPostParaCriarUmNovoCampeaoComOhSeguinteCorpo(String httpMethod,
                                                                                      List<ChampionDto> championDtos)
            throws Exception {

        log.info(httpMethod);
        log.info(championDtos.toString());

        ChampionDto body = championDtos.stream().findFirst().orElse(null);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.request(httpMethod, URI.create("/champion"))
                .characterEncoding(StandardCharsets.UTF_8)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(toJson(body));

        mvcResult = mockMvc.perform(request).andDo(print()).andReturn();

    }

    @Então("deverá retorna o http status {int}")
    public void deveraRetornarOhHttpStatus(Integer status) {
        log.info(status.toString());
        Assertions.assertEquals(status, mvcResult.getResponse().getStatus());
    }

    @E("corpo de resposta:")
    public void corpoDeResposta(List<ChampionDto> championDtos)
            throws UnsupportedEncodingException, JsonProcessingException {

        log.info(championDtos.toString());

        ChampionDto expectedBody = championDtos.stream().findFirst().orElseThrow();

        log.info("Expected body: {}", expectedBody);

        Champion champion = new ObjectMapper().readValue(mvcResult.getResponse().getContentAsString(), Champion.class);

        Assertions.assertNotNull(champion.getId());
        Assertions.assertEquals(expectedBody.getName(), champion.getName());
        Assertions.assertEquals(expectedBody.getDescription(), champion.getDescription());
        Assertions.assertEquals(expectedBody.getDifficulty(), champion.getDifficulty());
        Assertions.assertEquals(expectedBody.getRole(), champion.getRole());
        Assertions.assertEquals(expectedBody.getRegion(), champion.getRegion());
        Assertions.assertNotNull(champion.getSkills());
        Assertions.assertNotNull(champion.getSkins());

    }

    @E("com as seguintes skills:")
    public void comAsSeguintesSkills(List<SkillDto> skills) {
        log.info(skills.toString());
        championDto.setSkills(skills);

    }

    @E("com as seguintes skins:")
    public void comAsSeguintesSkins(List<SkinDto> skins) {
        log.info("{}", skins);
        championDto.setSkins(skins);
    }

    @Dado("o seguinte campeão:")
    public void oSeguinteCampeao(List<ChampionDto> champions) {
        log.info("{}", champions);
        championDto = champions.stream().findFirst().orElseThrow();

    }

    @Quando("for feita a requisição do tipo {string} para criar um novo campeão")
    public void forFeitaARequisicaoDoTipoParaCriarUmNovoCampeao(String httpMethod) throws Exception {
        log.info(httpMethod);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .request(httpMethod, URI.create("/champion"))
                .characterEncoding(StandardCharsets.UTF_8)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(championDto));

        mvcResult = mockMvc.perform(request).andDo(print()).andReturn();
    }
}
